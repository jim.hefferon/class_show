#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Compare LaTeX classes in a simple way.
"""
__version__ = '0.5.0'
__author__ = 'Jim Hefferon'
__license__ = 'GPL 3'

import sys, os, os.path, re, pprint, argparse, traceback, time
import tempfile, subprocess 
import resource
import csv

# https://github.com/olivierverdier/pydflatex
from pydflatex import LogProcessor

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = True
DEBUG = True
SOURCE_DIR = os.path.dirname(os.path.realpath(__file__))

class PTException(Exception):
    pass


def warn(s):
    t = 'WARNING: '+s+"\n"
    sys.stderr.write(t)
    sys.stderr.flush()

def error(s):
    t = 'ERROR! '+s
    sys.stderr.write(t)
    sys.stderr.flush()
    sys.exit(10)

def exception_happened(s,err):
    t = 'EXCEPTION! '+s+': '+str(err)
    print(t)

# This is the document that gets run
DUMMY = r"""\documentclass{{{class}}}

{titlepage_data} 
\usepackage{{lipsum}}
\begin{{document}}
{titlepage_declaration}
\lipsum[1]
{top_level_division_1}
\lipsum[2-3]
{top_level_division_2}
\lipsum[4-5]
{mid_level_division_1}
\lipsum[6-7]
{mid_level_division_2}
\lipsum[8-9]
{low_level_division_1}
\lipsum[10-11]
{low_level_division_2}
\lipsum[12-13]
\end{{document}}
"""


#==================================================================
def setlimits():
    # Set maximum CPU time in child process, after fork() but before exec()
    if DEBUG:
        print("Setting resource limit in child (pid {0})".format(os.getpid()))
    # resource.setrlimit(resource.RLIMIT_CPU, (.05, .05))  # CPU time in secs
    # resource.setrlimit(resource.RLIMIT_FSIZE, (50000, 50000)) # file size
    # resource.setrlimit(resource.RLIMIT_NOFILE, (10, 10)) # Number open files
    resource.setrlimit(resource.RLIMIT_CPU, (1.05, 1.05))  # CPU time in secs
    resource.setrlimit(resource.RLIMIT_FSIZE, (500000, 500000)) # file size
    resource.setrlimit(resource.RLIMIT_NOFILE, (10, 10)) # Number open files

def run_latex(s, number_runs=2, timeout=5):
    """Run LaTeX on the given string.  
      s  string  Contains LaTeX source
    Return a list of three (status,pdffile string,logfile string).  If status=0
    then the process ran normally. If status=1 then there was an error.  The
    strings may be None, if this routine got confused. 
    """
    r = [None,None,None]   # return value
    # Dump output?
    if VERBOSE:
        DEV_NULL = ''
    else:
        DEV_NULL = " >/dev/null"
    # if DEBUG and VERBOSE:
    print("==> run_latex(): s={0!s}".format(s))
    starting_dir = os.getcwd()
    SET_TEXMFCNF = "TEXMFCNF="+SOURCE_DIR+"; " # make LaTeX use the special .cnf
    # SET_TEXMFCNF = ''
    if DEBUG:
        print("SET_TEXMFCNF is {0!s}".format(SET_TEXMFCNF))
    with tempfile.TemporaryDirectory() as tmpdirname:
        if DEBUG:
            print("==> run_latex(): tmpdirname={0!s}".format(tmpdirname))
        os.chdir(tmpdirname)
        (fd,fn) = tempfile.mkstemp(suffix='tex',dir=tmpdirname)
        if DEBUG:
            print("==> run_latex(): opening file name fn={0!s}".format(fn))
        try:
            f = open(fn,'w')
        except Exception as err:
            exception_happened('while opening temp file',err)
        try:
            f.write(s)
        except Exception as err:
            exception_happened('while writing to temp file',err)
        try:
            f.close()
        except Exception as err:
            exception_happened('while closing temp file',err)
        if DEBUG:
            print("==> run_latex(): source file written")
        fn_without_tex = os.path.splitext(fn)[0]
        for run_no in range(0,number_runs):
            rc = None
            try:
                # Run LaTeX (it does not say -file-line-error because 
                #    the error parser does not like it)
                cmd_line = SET_TEXMFCNF+"pdflatex -interaction nonstopmode -no-shell-escape -no-parse-first-line -no-mktex tfm "+fn_without_tex+DEV_NULL
                # cmd_line = "pdflatex -interaction nonstopmode -no-shell-escape -no-parse-first-line -no-mktex tfm "+fn_without_tex+DEV_NULL
                if VERBOSE:
                    print("cmd_line is -->{0!s}".format(cmd_line))
                rc = subprocess.call([cmd_line], shell=True, preexec_fn=setlimits, executable='/bin/bash') 
            except Exception as err:
                exception_happened('while running LaTeX',str(err))
                rc = -1
                break
            if DEBUG:
                print("==> run_latex(): LaTeX finished with rc={0} for run_no {1}".format(rc,run_no))
            r[0] = rc
            if rc>1:
                break
            else:
                try:
                    log_fn = fn_without_tex+'.log'
                    log_f = open(log_fn,'rb')
                    log_contents = log_f.read()
                    r[2] = log_contents.decode("utf-8") # make it a string
                    log_f.close()
                except Exception as err:
                    exception_happened('while gathering log file', err)
        else:
            if DEBUG:
                print("==> run_latex(): LaTeX successfully run")
            if rc == 0:
                try:
                    pdf_fn = fn_without_tex+'.pdf'
                    pdf_f = open(pdf_fn,'rb')
                    r[1] = pdf_f.read()
                    pdf_f.close()
                except Exception as err:
                    exception_happened('while gathering pdf file',err)
        # Clean up
        try:
            os.close(fd)  # free up file handle
        except Exception as err:
            exception_happened('while freeing the file handle',err)
        try:
            os.chdir(starting_dir)
        except Exception as err:
            exception_happened('while changing the dir back',err)
    return r


def get_error(s):
    """Get the error message from the LaTeX log.  
      s  string  Contents of log file
    Returns None (if a total failure happens) or a dictionary containing the 
    error information.  Keys are: 'file' (name of file), 
    'code' (offending line), 'line' (line number as string), 'kind' ("error", 
    "box", "ref", or "warning"), 'text' (text of error).  For more see line
    112 of latexlogparser.py.
    """
    r = None   # return value
    if DEBUG and VERBOSE:
        print("==> get_error(): s={0!s}".format(s))
    starting_dir = os.getcwd()
    with tempfile.TemporaryDirectory() as tmpdirname:
        if DEBUG:
            print("==> get_error(): tmpdirname={0!s}".format(tmpdirname))
        os.chdir(tmpdirname)
        (fd,fn) = tempfile.mkstemp(suffix='log',dir=tmpdirname)
        if DEBUG:
            print("==> get_error(): opening file name fn={0!s}".format(fn))
        # Write the log file, because the parser only handles files
        try:
            f = open(fn,'w')
        except Exception as err:
            exception_happened('while opening error temp file',err)
        try:
            f.write(s)
        except Exception as err:
            exception_happened('while writing to error temp file',err)
        try:
            f.close()
        except Exception as err:
            exception_happened('while closing error temp file',err)
        if DEBUG:
            print("==> get_error(): error file written")
        try:
            log_processor = LogProcessor(options={'colour':False, 'debug':False})
            r = log_processor.process_log(fn)
        except Exception as err:
            exception_happened('while processing error temp file',err)
        else:
            if DEBUG:
                print("==> get_error(): LaTeX successfully run")
        # Clean up
        try:
            os.close(fd)  # free up file handle
        except Exception as err:
            exception_happened('while freeing the error file handle',err)
        try:
            os.chdir(starting_dir)
        except Exception as err:
            exception_happened('while changing the error dir back',err)
    return r

# =====================
def read_input(fn='class_show.csv'):
    """Read in the CSV input. Number of fields should match the number
    of fields in DUMMY. 
    """
    r = []
    with open(fn, newline='') as csvfile:
        rdr = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in rdr:
            d = {}  
            d['class'] = row[0]
            d['titlepage_data'] = row[1]
            d['titlepage_declaration'] = row[2]
            d['top_level_division_1'] = row[3]
            d['top_level_division_2'] = row[4]
            d['mid_level_division_1'] = row[5]
            d['mid_level_division_2'] = row[6]
            d['low_level_division_1'] = row[7]
            d['low_level_division_2'] = row[8]
            r.append(d)
    return r


# ================================
def main(args):
    for d in read_input():
        fn = d['class']
        print("---- running {0:s}.pdf ----".format(fn))
        status,pdf,log = run_latex(DUMMY.format(**d))
        if status == 0:
            f = open('{0:s}.pdf'.format(fn), 'wb')
            f.write(pdf)
            f.close()
        elif status >1:
            print("ERROR: you've exceeded the system limits this program has set.  You can enlarge them.")
        else:
            error_dct = get_error(log)
            # print("ERROR: {code}\n  line={line}\n message={text}".format(str()))
            print("ERROR: {code}\n  line={line}\n message={text}".format(**error_dct))
    print("+++done+++++")

#==================================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description=globals()['__doc__'])
        parser.add_argument('-v','--version', action='version', version='%(prog)s '+globals()['__version__'])
        parser.add_argument('-D', '--debug', action='store_true', default=False, help='run debugging code')
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        args = vars(args)
        if ('debug' in args) and args['debug']: 
            DEBUG = True
        if ('verbose' in args) and args['verbose']: 
            VERBOSE = True
        main(args)
        if VERBOSE: 
            print('elapsed: {0:2f} seconds'.format(time.time()-start_time))
    except KeyboardInterrupt: # Ctrl-C
        print("Keyboard interruption")
    except SystemExit: # sys.exit()
        print("System exit")
        traceback.print_exc()
    except Exception as e:
        print('UNEXPECTED OUTCOME')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
    else:
        sys.exit(0)
